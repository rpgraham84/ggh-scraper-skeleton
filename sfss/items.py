# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
import scrapy

from operator import itemgetter


class ProductItem(scrapy.Item):
    """
    Represents a product to be loaded into the SFS database of products.
    """
    # FIXME: Remove this debug crap!
    children = scrapy.Field()

    #: MANDATORY
    #: The name of the product.
    #:
    #: Sometimes, this will have to be simply the maker and upc concatenated
    #: with a space, but will *ideally* be what the original manufacturer
    #: calls the product.
    title = scrapy.Field()

    #: MANDATORY
    #: Maker of the product.
    maker = scrapy.Field()

    #: MANDATORY
    #: Unique identifier of the product (UPC or SKU).
    #:
    #: Occasionally, there will be products that don't already have a UPC. In
    #: that case, you must generate a unique identifier for each product.
    upc = scrapy.Field()

    #: MANDATORY FOR CHILD IMPOSSIBLE FOR PARENT
    #: UPC of the parent product if the product has one, or None otherwise.
    canonical_upc = scrapy.Field()

    #: One-line description likely containing manufactuer-specific verbage.
    #:
    #: Strive to include the product line. This field becomes extra important
    #: when the title must be a maker + upc concatenation.
    blurb = scrapy.Field()

    #: MANDATORY
    #: Very high-level conceptual type for the product.
    #:
    #: Generally, this can be described as the response you would give if a
    #: five-year-old asked, with the one exception being when the answer to
    #: "is it something you seen often?" is no (e.g., a table with a
    #: large circular hole cut out of it would be a grill table, not a table).
    class_ = scrapy.Field()

    #: MANDATORY
    #: List of breadcrumb categories in which this product should exist.
    #:
    #: Breadcrumbs are separated by " > ". For example,
    #:
    #:     categories = [
    #:         "Kitchen > Cooking > Ranges",
    #:         "Outdoor Living > Cooking > Ranges"
    #:     ]
    categories = scrapy.Field()

    #: The price of the product as a float or a string.
    price = scrapy.Field()

    #: One or more paragraphs of text that describe the product.
    description = scrapy.Field()

    #: Optionally named lists of the features of this product.
    #:
    #: This holds descriptive information that doesn't fit into either the
    #: paragraph or name-value paradigms. Often this is in the form of one or
    #: more bulleted lists that sometimes have header text. For example:
    #:
    #:     PROGRAMS
    #:
    #:     Heavy wash
    #:     Quick wash
    #:     Eco wash
    #:     Rinse and hold
    #:
    #:     OPTIONS
    #:     High temperature
    #:     Long dry
    #:     Time Saver
    #:
    #:     CLEANING SYSTEMS
    #:     7
    #:     Self cleaning filters
    #:     AquaLevel™ Sensor
    #:
    #:     COMFORT/EASY OF USE
    #:     Fault indication
    #:     Rinse-aid indication
    #:     Time remaining indicator
    #:
    #:     SAFETY
    #:     AquaSafe™
    #:     SteamSafe™
    #:     KidLock™ - lock the door
    #:
    #:     BASKETS
    #:     Upper: Standard
    #:     Lower: Premium
    #:
    #: would translate into the following features:
    #:
    #:     features = {
    #:         "Programs": [
    #:             "Heavy wash",
    #:             "Quick wash",
    #:             "Eco wash",
    #:             "Rinse and hold"
    #:         ],
    #:         "Options": [
    #:             "High temperature",
    #:             "Long dry",
    #:             "Time Saver"
    #:         ],
    #:         "Cleaning Systems": [
    #:             "7",
    #:             "Self cleaning filters",
    #:             "AquaLevel™ Sensor"
    #:         ],
    #:         "Comfort/Easy of Use": [
    #:             "Fault indication",
    #:             "Rinse-aid indication",
    #:             "Time remaining indicator"
    #:         ],
    #:         "Safety": [
    #:             "AquaSafe™",
    #:             "SteamSafe™",
    #:             "KidLock™ - lock the door"
    #:         ],
    #:         "Baskets": [
    #:             "Upper: Standard",
    #:             "Lower: Premium"
    #:         ]
    #:     }
    features = scrapy.Field()

    #: Attributes and their values as specified for the class of the product.
    #:
    #: Each attribute is composed of a name, code, and value. The code becomes
    #: a python attribute, and therefore must be valid as a python variable
    #: name. The name is the customer-facing name for the attribute, and the
    #: value is, of course, the value the product has for the attribute.
    #:
    #: These details are specified as {"code": ("Name", "value")}. For example,
    #:
    #:     attributes = {
    #:         "weight": ("Weight", "56 lbs."),
    #:         "finish": ("Finish", "Stainless Steel"),
    #:         "is_ada": ("ADA Compliance", True)
    #:     }
    attributes = scrapy.Field()

    #: Information about the color(s) of the product.
    #:
    #: The colors field should be a list of dictionaries each describing one
    #: color. Colors must have a name, and may also specify a hexcode and/or a
    #: url for a swatch image.
    #:
    #: !! NOTE !!
    #: Don't forget to append the swatch image url to the list of image_urls
    #: otherwise it won't be fetched by scrapy!
    #:
    #: Here are some valid color examples:
    #:
    #:      colors = [{
    #:              "name": "British Green"
    #:          },{
    #:              "name": "Pearlescent Blue",
    #:              "swatch_url": "http://www.example.com/swatches/pblue.jpg"
    #:          },{
    #:              "name": "Grey",
    #:              "hexcode": "AAAaaa"
    #:          },{
    #:              "name": "Panther Pink",
    #:              "swatch_url": "http://www.example2.com/img/pp.png",
    #:              "hexcode": "f58092"
    #:          }
    #:      ]
    #:
    #:      colors = []
    #:
    #:      colors = [{
    #:              "name": "Pumpkin",
    #:              "hexcode": "ff7619"
    #:          }
    #:      ]
    #:
    #: Industry specific logic dictates, however, that if the finish of a
    #: product is panel-ready, overlay, or integrated then the product *should*
    #: have no color information (these types of finishes really ought to be
    #: called "unfinishes" since they're actually specific ways leaving the
    #: finish on the product to the builder/renovator/customer).
    colors = scrapy.Field()

    #: MANDATORY
    #: Boolean indicating whether the product can be sold online.
    #:
    #: This will be true (at the moment) only for Gütesiegel and Grilldome
    #: products.
    is_basketable = scrapy.Field()

    #: MANDATORY
    #: Boolean indicating whether the price of the product can be shown online.
    #:
    #: This will be true (at the moment) only for Gütesiegel, Grilldome, and
    #: Guy's Deals' products.
    is_price_viewable = scrapy.Field()

    #: A URL at which this product can be viewed.
    url = scrapy.Field()

    #: Dictionary of file download URLs for the product and associated info.
    #:
    #: Types of documents
    #: ------------------
    #:
    #: Each download must be categorized as one of the following document types:
    #:
    #:  - Quick Reference (code='REF')
    #:  - Use & Care (code='UAC')
    #:  - Installation (code='INS')
    #:  - 2D CAD Drawing (code='C2D')
    #:  - 3D CAD Drawing (code='C3D')
    #:  - Design Guide (code='DES')
    #:  - 20/20 Catalog (code='CAT')
    #:  - Other (code='OTH')
    #:
    #: 2D and 3D CAD Drawings are generally identifiable as such. In cases
    #: where no distinction is made, we assume the vacuous truth: it's 3D.
    #:
    #: 20/20 Catalog is an industry specific collection of 3D models for
    #: products. Generally, each manufacturer has a download several megabytes
    #: in size that contains their entire contribution to the catalog (i.e., all
    #: of their products). However, some manufacturers may provide several links
    #: where each link is the download for a single product or a small set of
    #: closely related products (e.g., left-hinge and right-hinge versions of
    #: the same model). In general, we simply associate any 20/20 Catalog
    #: download with the product in which the download was found.
    #:
    #: Quick Reference documents are generally one or two pages long and specify
    #: the most often desired specifications: features, size, installation
    #: depictions, etc.
    #:
    #: Use & Care documents come in several varieties and may not actually
    #: mention the phrase "use and care." However, if the document pertains to
    #: actions that must be repeated during the lifetime of the product, then it
    #: can probably be considered a use and care guide (e.g., "Maintenance
    #: Guide", "Cleaning Instructions", etc.).
    #:
    #: Installation documents are usually readily identifiable as such by their
    #: inevitable mention of the term "installation" (but remain vigilant, of
    #: course).
    #:
    #: Design Guides are a particular kind of document that is pretty much
    #: specific to the appliance, renovation, and design industries. It is a
    #: specification of inter-product compatabilities that often showcases
    #: curated collections of products from the manufacturer.
    #:
    #: If the download cannot be classified into one of the above types, then by
    #: default it is classified as Other.
    #:
    #: Data format for file_urls
    #: -------------------------
    #:
    #: Once the document type has been determined, the code for the document
    #: type and a name for the document must be specified in a dictionary. The
    #: dictionary is then stored with the download URL as its key in file_urls.
    #:
    #: For example, if the following download was the only download for a
    #: product:
    #:
    #:      URL: "http://example.com/some_document.pdf"
    #:      type: Quick Reference
    #:      name: "XYZ Quick Ref Guide"
    #:
    #: then the entire file_urls value would be the following dictionary:
    #:
    #:      {
    #:          "http://example.com/some_document.pdf": {
    #:              "name": "XYZ Quick Ref Guide",
    #:              "kind": "REF"
    #:          }
    #:      }
    #:
    file_urls = scrapy.Field()

    #: List of URLs pointing to images for the product.
    image_urls = scrapy.Field()

    #: MANDATORY if any file_urls are present
    #: Details about each file that was downloaded successfully.
    #:
    #: This field is given a value in the pipeline, so simply ignore it during
    #: the scrape.
    files = scrapy.Field()

    #: MANDATORY if any image_urls are present
    #: Details about each image that was downloaded successfully.
    #:
    #: This field is given a value in the pipeline, so simply ignore it during
    #: the scrape.
    images = scrapy.Field()

    def set_attr(self, code, name, value, delete=None):
        if delete is not None:
            del self['attributes'][delete]
        self['attributes'][code] = name, value

    def del_attr(self, code):
        del self['attributes'][code]

    def attr_val(self, code, default=None):
        if code not in self['attributes']:
            return default
        return self['attributes'][code][1]
