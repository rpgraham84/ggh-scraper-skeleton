# -*- coding: utf-8 -*-

# Scrapy settings for sfss project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
# http://doc.scrapy.org/en/latest/topics/settings.html
#
import os

BOT_NAME = 'sfss'

SPIDER_MODULES = ['sfss.spiders']
NEWSPIDER_MODULE = 'sfss.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'sfss (+http://www.yourdomain.com)'

# pipeline: index (order)
ITEM_PIPELINES = {
    'sfss.pipelines.DuplicateUpcPipeline': 400,
    'scrapy.contrib.pipeline.images.ImagesPipeline': 600,
    'scrapy.contrib.pipeline.files.FilesPipeline': 800,
    'sfss.pipelines.CategorizingPipeline': 999,
}

BASE_DIR = "scrape_data"
IMAGE_DIR = "images"
FILE_DIR = "files"

# Directory in which to store images.
IMAGES_STORE = os.path.join(BASE_DIR, IMAGE_DIR)

# Directory in which to store other files.
FILES_STORE = os.path.join(BASE_DIR, FILE_DIR)
