# -*- coding: utf-8 -*-
from .text import slugify


class Categorizer(object):
    """
    A universal product categorizer.

    Currently, all product classes from the following manufacturers have been
    implemented:
     - Asko
     - Bosch
     - Gütesiegel
     - Subzero-Wolf
     - BainUltra
     - Miele*

     * in progress
    """
    def categorize(self, item):
        klass = slugify(item['class_'])
        handler = getattr(self, 'handle_%s' % klass, None)
        try:
            item['categories'] = list(handler(item))
        except TypeError:
            raise ValueError('%s is an unknown class' % item['class_'])

    def handle_airjet_bath(self, item):
        yield 'Bath > Relaxation > Therapeutic Airjet Baths'

    def handle_bathtub(self, item):
        yield 'Bath > Relaxation > Therapeutic Baths'

    def handle_beverage_center(self, item):
        yield 'Kitchen > Refrigeration > Beverage Centers'

    def handle_blower(self, item):
        yield 'Kitchen > Ventilation > Blowers'

    def handle_coffee_maker(self, item):
        if item['attributes']['type'] == "Built-in":
            yield 'Kitchen > Cooking > Built-in Coffee Makers'
        else:
            yield 'Kitchen > Essentials > Countertop Appliances'

    def handle_combiset(self, item):
        yield 'Kitchen > Cooking > Combisets'

    def handle_cooktop(self, item):
        if item['attributes'].get('is_outdoor'):
            yield 'Outdoor Living > Cooking > Cooktops'
        yield 'Kitchen > Cooking > Cooktops'

    def handle_cutting_board(self, item):
        yield 'Kitchen > Knives > Accessories'

    def handle_dishwasher(self, item):
        if item['attributes'].get('is_outdoor'):
            yield 'Outdoor Living > Cleaning > Dishwashers'
        yield 'Kitchen > Cleaning > Dishwashers'

    def handle_dryer(self, item):
        yield 'Laundry > Washers & Dryers > Dryers'

    def handle_grill(self, item):
        yield 'Outdoor Living > Cooking > Grills'

    def handle_ice_maker(self, item):
        if item['attributes'].get('is_outdoor'):
            yield 'Outdoor Living > Refrigeration > Ice Machines'
        yield 'Kitchen > Refrigeration > Ice Machines'

    def handle_iron(self, item):
        yield 'Laundry > Ironing > Irons'

    def handle_kitchen_shears(self, item):
        yield 'Kitchen > Knives > Accessories'

    def handle_knife(self, item):
        yield 'Kitchen > Knives > Individual Knives'

    def handle_knife_set(self, item):
        yield 'Kitchen > Knives > Knife Sets'

    def handle_microwave(self, item):
        try:
            type_ = item['attributes']['type'][1]
        except (KeyError, IndexError):
            return
        if type_ == "Built-in":
            yield 'Kitchen > Cooking > Built-in Microwaves'
        else:
            yield 'Kitchen > Essentials > Countertop Appliances'

    def handle_module(self, item):
        if item['attributes'].get('is_outdoor'):
            yield 'Outdoor Living > Cooking > Modules'
        yield 'Kitchen > Cooking > Modules'

    def handle_oven(self, item):
        yield 'Kitchen > Cooking > Ovens'

    def handle_range(self, item):
        yield 'Kitchen > Cooking > Ranges'

    def handle_rangetop(self, item):
        yield 'Kitchen > Cooking > Rangetops'

    def handle_refrigeration(self, item):
        if item['attributes'].get('is_outdoor'):
            yield 'Outdoor Living > Refrigeration > Refrigerators & Freezers'
        yield 'Kitchen > Refrigeration > Refrigerators & Freezers'

    def handle_sharpener(self, item):
        yield 'Kitchen > Knives > Accessories'

    def handle_sheath(self, item):
        yield 'Kitchen > Knives > Accessories'

    def handle_shelf(self, item):
        yield 'Laundry > Essentials > Shelves'

    def handle_vacuum(self, item):
        try:
            series = item['attributes']['series'][1].lower()
        except (KeyError, IndexError):
            yield 'Laundry > Vacuums > Upright'
            return

        if 'upright' in series:
            yield 'Laundry > Vacuums > Upright'
        if 'canister' in series:
            yield 'Laundry > Vacuums > Canister'
        if 'drone' in series:
            yield 'Laundry > Vacuums > Drone'
        if 'central' in series:
            yield 'Laundry > Vacuums > Central'

    def handle_ventilation(self, item):
        yield 'Kitchen > Ventilation > Vent Hoods'

    def handle_warming_drawer(self, item):
        if item['attributes'].get('is_outdoor'):
            yield 'Outdoor Living > Cooking > Warming Drawers'
        yield 'Kitchen > Cooking > Warming Drawers'

    def handle_washer(self, item):
        yield 'Laundry > Washers & Dryers > Washers'

    def handle_wine_coolers(self, item):
        if item['attributes'].get('is_outdoor'):
            yield 'Outdoor Living > Refrigeration > Wine Coolers'
        yield 'Kitchen > Refrigeration > Wine Coolers'

    def handle_wine_storage(self, item):
        if item['attributes'].get('is_outdoor'):
            yield 'Outdoor Living > Refrigeration > Wine Coolers'
        yield 'Kitchen > Refrigeration > Wine Coolers'
