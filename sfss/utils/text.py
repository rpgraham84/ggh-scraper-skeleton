import re
import unicodedata


def slugify(value):
    value = unicodedata.normalize('NFKD', unicode(value))
    value = value.encode('ascii', 'ignore')
    value = value.decode('ascii', 'ignore')
    value = re.sub('[^\w\s_-]', '', value).strip().lower()
    return re.sub('[-\s]+', '_', value)
