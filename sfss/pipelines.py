# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import re
import magic
import mimetypes

import warnings
import hashlib

from StringIO import StringIO

from .utils.text import slugify
from .utils.categorizer import Categorizer
from scrapy.utils.misc import md5sum
from collections import defaultdict, Counter

from scrapy.http import Request, Response
from scrapy.exceptions import DropItem
from scrapy.shell import inspect_response
from scrapy.contrib.pipeline.images import ImagesPipeline
from scrapy.contrib.pipeline.files import FilesPipeline


class DuplicateUpcPipeline(object):
    def __init__(self):
        self.upc_counter = Counter()
        self.items = {}
        self.url_counter = Counter()

    def process_item(self, item, spider):
        upc = item['upc']
        if upc in self.items:
            # We've seen this UPC before. Compare other fields to determine
            # if we have actually seen it before or if the UPCs are simply
            # the same. If all fields match, drop this item. Otherwise,
            # return it after making the UPC unique.
            original = self.items[upc]
            if original == item:
                raise DropItem('Already seen {}'.format(upc))
            else:
                item['upc'] = self.make_unique(upc)
                return item
        else:
            # New item - count the UPC and return as is.
            self.count(item)
            return item

    def make_unique(self, upc):
        uupc = '{}--{!s}'.format(upc, self.upc_counter[upc])
        self.upc_counter[upc] += 1
        return uupc

    def count(self, item):
        upc = item['upc']
        self.items[upc] = item
        self.upc_counter[upc] += 1


class CategorizingPipeline(object):
    categorizer = Categorizer()

    def process_item(self, item, spider):
        try:
            self.categorizer.categorize(item)
        except ValueError:
            pass
        return item


class SEOImagesPipeline(ImagesPipeline):
    #: Record of which images belong to which items.
    items_by_image_id = defaultdict(list)

    #: Record of times a filename has been used before.
    use_count = Counter()

    def get_media_requests(self, item, info):
        for url in item.get(self.IMAGES_URLS_FIELD, []):
            # Be sure to record which items have a given image.
            self.items_by_image_id[url].append(item)
            yield Request(url)

    def _create_new_filename(self, url):
        # Use the distinct values from all items for maker, upc, and class to
        # create the base filename.
        items = self.items_by_image_id[url]
        idents = _get_csv(items, 'maker', 'upc', 'class_')
        basename = _remove_slashes('__'.join(idents))

        # Make sure to create unique filenames by appending the number of uses
        # of the base filename thus far before returning it.
        nid = self.use_count[basename]
        self.use_count[basename] += 1
        return '{}__{}.jpg'.format(basename, nid)

    def file_path(self, request, response=None, info=None):
        """Full-sized images are saved as 'full/<filename>'"""
        url = request if not isinstance(request, Request) else request.url
        filename = self._create_new_filename(url)
        return 'full/{}'.format(filename)

    def thumb_path(self, request, thumb_id, response=None, info=None):
        """Thumbnails are saved as 'thumbs/<thumbnail_name>/<filename>'"""
        url = request if not isinstance(request, Request) else request.url
        filename = self._create_new_filename(url)
        return 'thumbs/{}/{}'.format(thumb_id, filename)


def _get_csv(items, *attrs):
    """For each attribute, yield the sorted csv of distinct values."""
    for attr in attrs:
        yield ','.join(sorted(set(i.get(attr) for i in items)))


def _remove_slashes(text, replacement='-'):
    return re.sub(r'/', replacement, text)


class FileRenamingPipeline(FilesPipeline):
    def get_media_requests(self, item, info):
        return [Request(x, meta={"filename": slugify("_".join([item['maker'], item['upc'], item['file_urls'][x]['name']]))}) for x in item.get(self.FILES_URLS_FIELD, [])]

    def file_path(self, request, response=None, info=None, buf="%PDF-1.4"):
        # start of deprecation warning block (can be removed in the future)
        def _warn():
            from scrapy.exceptions import ScrapyDeprecationWarning

            warnings.warn('FilesPipeline.file_key(url) method is deprecated, please use '
                          'file_path(request, response=None, info=None) instead',
                          category=ScrapyDeprecationWarning, stacklevel=1)
        # check if called from file_key with url as first argument
        if not isinstance(request, Request):
            _warn()
            url = request
        else:
            url = request.url

        # detect if file_key() method has been overridden
        if not hasattr(self.file_key, '_base'):
            _warn()
            return self.file_key(url)
        # end of deprecation warning block

        #media_guid = hashlib.sha1(url).hexdigest()  # change to request.url after deprecation
        media_guid = request.meta['filename']
        try:
            media_ext = mimetypes.guess_extension(magic.from_buffer(buf, mime=True))
        except Exception:
            if not response:
                response = Response()
            response.meta['locals'] = locals()
            inspect_response(response)

        return 'full/%s%s' % (media_guid, media_ext)

    def file_downloaded(self, response, request, info):
        path = self.file_path(request, response=response, info=info, buf=response.body)
        self.store.persist_file(path, StringIO(response.body), info)
        checksum = md5sum(StringIO(response.body))
        return checksum

    # deprecated
    def file_key(self, url):
        return self.file_path(url)
    file_key._base = True
