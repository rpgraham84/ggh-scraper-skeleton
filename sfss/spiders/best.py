# -*- coding: utf-8 -*-
import scrapy
from scrapy import FormRequest, Request, log
from sfss.items import ProductItem
from scrapy.shell import inspect_response


class BestSpider(scrapy.Spider):
    name = "Best"
    allowed_domains = ["bestrangehoods.com"]
    start_urls = (
        'http://www.bestrangehoods.com',
    )

    def parse(self, response):
        product_urls = response.xpath(
            '//*[@id="ctl00_ctl00_radMainMenu_RadMenu1"]/ul/li[1]/div/ul/li/a/@href').extract()
        categories = response.css(
            "#ctl00_ctl00_radMainMenu_RadMenu1 > ul > li.rmItem.rmFirst > div > ul > li.rmItem a > span::text").extract()
        categories = categories = [cat.split()[0].lower() for cat in categories]
        for url, category in zip(product_urls, categories):
            yield Request(url, meta={"product_category": category}, callback=self.visit_product_list)

    def parse_form_response(self, response):
        desired_page = response.meta["product_category"]
        current_page = response.css(
            'input[src="../../App_Themes/Main/images/icon_delete.gif"]').extract()
        if len(current_page) > 0:
            current_page = current_page[0].lower()
        else:
            current_page = ""

        # Sometimes (a lot), bestrangehoods likes to return a different page than the
        # one we requested. It's one of the weirdest anomalies ever.
        # This code deals with that by checking to see if the name of what we
        # requested matches what we got in the page and if it doesn't,
        # we requeue the request until best returns what we asked for.
        if not current_page.lower().find(desired_page.lower()):
            yield Request(response.url, meta={"product_category": desired_page}, dont_filter=True,
                          callback=self.visit_product_list)
            log.msg("current_page: {0}, desired_page: {1}".format(current_page, desired_page), level=log.WARNING)
            return

        product_urls = ['http://www.bestrangehoods.com/store/products/' + u for u in
                        response.css(".productDetail a::attr(href)").extract()]

        for product_url in product_urls:
            yield Request(product_url, meta={"product_category": desired_page}, dont_filter=True,
                          callback=self.parse_product_page)

    def visit_product_list(self, response):
        category = response.meta['product_category']
        yield FormRequest.from_response(
            response,
            meta={"product_category": category},
            formdata={
                "ctl00$ctl00$ContentPlaceHolderBaseBody$ContentPlaceHolderBody$ddlResultsPerPage": "_0",
                "__EVENTTARGET": "ctl00$ctl00$ContentPlaceHolderBaseBody$ContentPlaceHolderBody$ddlResultsPerPage"
            },
            callback=self.parse_form_response)

    def parse_product_page(self, response):
        item = ProductItem()
        image_urls = ["http://www.bestrangehoods.com" + u for u in
                      response.css(".imgTabThumb img::attr(src)").extract()]

        if len(response.css("#ctl00_ctl00_ContentPlaceHolderBaseBody_ContentPlaceHolderBody_tcDescriptors_body").xpath(
                "//img[contains(@src, 'Reference_Sheets')]/@src").extract()) > 1:
            image_urls.append("http://www.bestrangehoods.com/" + response.css(
                "#ctl00_ctl00_ContentPlaceHolderBaseBody_ContentPlaceHolderBody_tcDescriptors_body").xpath(
                "//img[contains(@src, 'Reference_Sheets')]/@src").extract()[1].split("../")[-1])

        item['url'] = response.url
        item['upc'] = response.css('#divProductDetail > div.rightcolumn > h1::text').extract()[0]
        item['class_'] = 'Ventilation'
        item['blurb'] = response.css("#prodcutshortdescription::text").extract()[0].strip()
        item['image_urls'] = image_urls

        yield item