import re

from urlparse import urlparse
from string import capwords
from urlparse import urljoin
from fractions import Fraction

from scrapy.shell import inspect_response
from scrapy.contrib.spiders import SitemapSpider

from sfss import utils
from sfss.items import ProductItem


class SubZeroWolfSitemap(SitemapSpider):
    name = 'Subzero-Wolf'
    domain = 'www.subzero-wolf.com'
    sitemap_urls = ['http://www.subzero-wolf.com/sitemap.xml']

    #: Sub-zero always has a width in the url of leaf product pages.
    #: Wolf follows the same pattern except with warming drawers.
    sitemap_rules = [
        # Subzero product url rules:
        # 1. r'''/sub-zero/''' since they all start with "/sub-zero/"
        # 2. r'''.*/''' since none exist as direct children of /sub-zero/
        # 3. r'''[^/]*\d+[^/]*$''' since each contains width in the last portion
        #    of the path
        ('/sub-zero/.*/[^/]*\d+[^/]*$', 'parse_subzero'),
        # Wolf product url rules:
        # 1. r'''/wolf/''' since they all start with "/wolf/"
        # 2. r'''.*/''' since none exist as direct children of /wolf/
        # 3. r'''[^/]*(\d+|warming)[^/]*$''' since each contains either the
        #    width or "warming" in the last portion of the path
        ('/wolf/.*/[^/]*(\d+|warming)[^/]*$', 'parse_wolf'),
        ]
    attrs_from_feature = {
        'A': {'high_alititude': ('High Alititude', True)},
        'HA': {'high_alititude': ('High Alititude', True)},
        'TH': {'handle_type': ('Handle Type', 'Tubular')},
        'PH': {'handle_type': ('Handle Type', 'Pro')},
        'LH': {'hinge': ('Hinge', 'Left')},
        'RH': {'hinge': ('Hinge', 'Right')},
        'P': {'has_pump': ('Pump', True)},
        'LP': {'gas_type': ('Gas Type', 'Liquid Propane')},
    }

    def absurl(self, path):
        return urljoin('http://%s' % self.domain, path)

    def parse_subzero(self, response):
        return self.basics(response, 'Sub-Zero')

    def parse_wolf(self, response):
        return self.basics(response, 'Wolf')

    def basics(self, response, maker):
        item = ProductItem()

        # Product maker.
        item['maker'] = maker

        # Product url.
        item['url'] = response.url

        # Product class.
        item['class_'] = '?'    # FIXME: Mere filler material

        # Product UPC.
        upc = response.css('h1 small::text')[0].extract()
        item['upc'] = upc

        # Product title.
        item['title'] = response.css('h1::text')[0].extract()

        # Is Basketable?
        item['is_basketable'] = False

        # Is Price Viewable?
        item['is_price_viewable'] = False

        # Primary product image.
        image = response.css("#specs picture.frame-1col__img img")
        srcset = image.css("::attr(srcset)")[0].extract()
        item['image_urls'] = [self.absurl(u) for u in srcset.split(', ')[:1]]

        # Grab the content sections.
        intro, features = response.css('main > section > .content-block')[:2]

        # Product blurb.
        blurb = capwords(intro.css('h2::text')[0].extract())
        item['blurb'] = blurb

        # Product description (from the intro section).
        item['description'] = u'\n'.join(intro.css('p::text').extract())

        # Product features (from the features section).
        f_items = features.css('ul.spec-list li')
        f_names = f_items.css('h3::text').extract()
        f_descriptions = [[d] for d in f_items.css('p::text').extract()]
        item['features'] = dict(zip(f_names, f_descriptions))

        # More features (called "highlights") exist in the specs section.
        specs = response.css('#specs .accordion-list--spec-view')

        for li in specs.xpath('./li'):
            lname = li.xpath('./a/text()')[0].extract().lower()
            content = li.xpath('./ul/li')
            if 'highlights' in lname:
                highlights = content.xpath('text()')
                item['features']['Highlights'] = highlights.extract()
            elif 'downloads' in lname:
                item['file_urls'] = self.process_downloads(content.xpath('a'))
            elif 'model options' in lname:
                models = content.css('::text').extract()
                # For some reason each list in children contains an extra empty
                # element (which we can ignore) so upcs/prices every 3 elements,
                # not the 2 you would intuitively expect.
                cupcs = map(unicode.strip, models[::3])
                cprices = map(unicode.strip, models[1::3])
                models = zip(cupcs, cprices)
                item['children'] = [c for c in cupcs if c != item['upc']]

        # Grab the price. Skip products that aren't yet available. The intro
        # section contains not one but two strong tags on those products.
        msrp = intro.css('strong::text').extract()
        if len(msrp) > 1:
            return

        tag, price = msrp[0].rsplit(' ', 1)
        item['price'] = price

        # TODO: At this point, we'll probably want to collect a bunch of text
        # from various key places on the page and determine the class of this
        # product. From there we can better assign product attributes.
        item['attributes'] = {}

        # Let's make some simple decisions up front to save on mistakes later.
        klass = None
        url = urlparse(response.url)
        leaf = url.path.split('/')[-1:][0]
        if 'ice-maker' in leaf and not ('freezer' in leaf or 'refrigerator' in leaf):
            klass = 'Ice Maker'
        elif '/range-hood' in url.path:
            klass = 'Ventilation'
        elif 'beverage center' in item['title'].lower():
            klass = 'Beverage Center'
        elif '/wine-cooler' in url.path:
            klass = 'Wine Storage'
        elif 'refriger' in url.path:
            klass = 'Refrigeration'
        elif '/ovens' in url.path:
            klass = 'Oven'
        elif '/cooktops-and-rangetops' in url.path:
            if 'range-top' in url.path.split('/'):
                klass = 'Rangetop'
            else:
                klass = 'Cooktop'
        elif 'warming-drawer' in url.path:
            klass = 'Warming Drawer'
        elif '/coffee-systems' in url.path:
            klass = 'Coffee Maker'
        elif '/microwave-ovens' in url.path:
            klass = 'Microwave'
        elif '/cooktop' in url.path:
            klass = 'Module'
        elif '/grill' in url.path:
            if 'grill' not in url.path.rsplit('/', 1)[-1:][0]:
                klass = 'Module'
            else:
                klass = 'Grill'
        elif '/ranges' in url.path:
            klass = 'Range'

        # Grab the width. Pretty universal on Subzero products.
        width = self._get_width(item, response)
        item['attributes']['width'] = 'Width', width

        all_dims = self._get_dims(item, response, dims='whd')

        def volume(dimple):
            volume = 1
            for d in [self._parse_dimension(d) for d in dimple[1:]]:
                volume *= d
            return volume

        dims = sorted(all_dims, key=volume)[-1]
        dimstr = ' x '.join(dims[1:])
        item['attributes'].update({
            'dimensions': ('Dimensions', dimstr)
        })

        item['colors'] = []

        # Pass the item off to a handler specifically for the class of the item.
        if klass is not None:
            attributize = getattr(self, 'handle_%s' % utils.text.slugify(klass))
            attributize(item, response, models)

            # Handle any children.
            if len(models) > 1:
                item['upc'] = '(%s)' % upc  # signify it's a parent product
                features = {u: utils.get_upc_diff(u, upc) for u, _ in models}

                # Because Natural Gas is identifiable by the omission of 'LP'
                # from the UPC and it's presence in a sibling product, we look
                # at all future children to determine whether we are dealing
                # with a product that uses gas at all. Same for pumps.
                is_gas = any('LP' in f for f in features.values())
                is_pump = any('P' in f for f in features.values())

                for upc, price in models:
                    child = ProductItem(**item)
                    del child['children']
                    child.update({
                        'canonical_upc': item['upc'],
                        'upc': upc,
                        'price': price,
                        'attributes': {}
                    })

                    # Update the attributes of the child
                    for f in features[upc]:
                        child['attributes'].update(self.attrs_from_feature[f])

                    # Handle Natural gas (since its represented in the UPC as
                    # the lack of 'LP')
                    if is_gas and 'LP' not in features[upc]:
                        child['attributes'].update(
                            {'gas_type': ('Gas Type', 'Natural Gas')}
                        )
                    # Same for pumps.
                    if is_pump and 'P' not in features[upc]:
                        child['attributes'].update(
                            {'has_pump': ('Pump', False)}
                        )
                    yield child
        yield item

    def process_downloads(self, links):
        def get_kind(name):
            lname = name.lower()
            kind = 'OTH'
            if 'quick reference' in lname:
                kind = 'REF'
            elif 'installation' in lname:
                kind = 'INS'
            elif 'design guide' in lname:
                kind = 'DES'
            elif 'use and care' in lname:
                kind = 'UAC'
            elif '2d' in lname:
                kind = 'C2D'
            elif '3d' in lname:
                kind = 'C3D'
            elif '20' in lname:
                kind = 'CAT'
            return kind

        file_urls = {}
        for link in links:
            url = self.absurl(link.css('::attr(href)').extract()[0])
            name = link.css('.doc-title::text').extract()[0]
            file_urls[url] = {'name': name, 'kind': get_kind(name)}
        return file_urls

    @classmethod
    def _get_dims(cls, item, response, dims='whd', sep='x', unit='"'):
        # Each dimension consists of a number or fraction (a scale), an optional
        # unit of measurement (a unit), and something to indicate which
        # dimension is being specified (an abbreviation).
        #
        #        2 1/2"H
        #
        #   2 1/2   "    H
        #   scale[unit][abbr]
        #
        # Also, multiple dimensions are seperated by an 'x' or a '-' (a
        # seperator).
        #
        #    3"W x 2"H x 5 1/2"D
        #
        # Lastly, each set of dimensions is prefaced by a title (a name).
        #
        #    Overall Dimensions: 2'W x 8'H x 4'D

        ## Build the regex to capture each dim seperately.
        assert dims  # at least one dimension must be specified
        scale = r'\d[\d. /]*'
        dimension = r'({n}{u}?\w?)'.format(n=scale, u=unit)
        seperator = r'\s*{}\s*'.format(sep)
        multiple_dims = seperator.join([dimension] * len(dims))
        name = r'(\w+(?:\s*\w+)*)'
        dimension_set = r'^\s*{}:\s*{}\s*$'.format(name, multiple_dims)

        # We'll look in the name:value text of the specs section.
        text = ' '.join(response.css('#specs ::text').extract())
        text_lines = [l for l in text.splitlines() if ':' in l]

        # Search and return all results.
        matches = []
        for line in text_lines:
            matches.extend(re.findall(dimension_set, line, re.I))
        return matches

    @staticmethod
    def _parse_dimension(dim, unit='"'):
        nstr = None
        if '"' in dim:
            nstr = dim.split(unit, 1)[:1][0]
        else:
            # Read up to the first char that can't be in a legit float.
            i = 0
            while dim[i] in '/ .0123456789':
                i += 1
            nstr = dim[:i]
        try:
            return float(sum(map(Fraction, nstr.split())))
        except Exception:
            raise ValueError('%r cannot be converted into a float' % dim)

    @staticmethod
    def _get_width(item, response):
        if '"' in item['title']:
            inches, _ = item['title'].split('"', 1)
            try:
                return inches + u'"'
            except Exception:
                pass
        elif 'inch' in response.url:
            try:
                _, last = response.url.rsplit('/', 1)
                inches, _ = last.split('-')
                return inches + u'"'
            except Exception:
                pass
        elif 'warming drawer' in item['title'].lower():
            return u'30"'
        elif 'pro 48' in item['title'].lower():
            return u'48"'
        return None

    @staticmethod
    def _get_element_count(item, response, depth=1):
        burners = None
        product_title = response.css('main header h1::text')[0].extract()
        try:
            # Method one: split the title.
            _, desc = product_title.split('-')
            if 'burners' in desc.lower():
                burners = int(desc.strip().split()[:1][0])
        except Exception:
            pass
        if not burners:
            # Method two: search the features.
            count_re = '\d+|%s' % '|'.join(utils.numbers)
            target_re = '(element|burner)s'
            extra_re = '(\s+[\w-]+){0,%d}' % depth

            pattern_ = r'(?P<n>%s)%s\s+%s'
            pattern = pattern_ % (count_re, extra_re, target_re)
            regex = re.compile(pattern, re.I)

            candidates = response.css('#intro,#specs,#features').css('::text')
            likely_text = ' '.join(candidates.extract()).lower()

            # Look for the mentioning of the burner/element count that is the
            # greatest.
            greatest = 0
            for card, _, _ in regex.findall(likely_text):
                try:
                    card = int(card)
                except ValueError:
                    card = utils.numbers.get(card, 0)
                if card > greatest:
                    greatest = card
            if greatest:
                burners = greatest
        if not burners:
            # Method three: disect the UPC.
            # Some cooktop UPCs contain the width followed by the number of
            # elements/burners (e.g., 30" w/2 burners might be 'CT302S/T').
            try:
                width = str(item['attributes']['width'][1])
                w = str(width)
                i = item['upc'].index(w) + len(w)
                burners = int(item['upc'][i])
            except Exception:
                pass
        if not burners:
            # Method four: base it on the width!
            # A loose relationship exists between the width of the cooktop and
            # the number of burners/elements:
            #   - 15" -> 2
            #   - 30" -> 4
            #   - 36" -> 5
            try:
                width = str(item['attributes']['width'][1])
                burners = {'15': 2, '30': 4, '36': 5}.get(width)
            except Exception:
                pass
        if not burners:
            # Method four: just give up and guess two
            burners = 2
        return burners

    @classmethod
    def handle_beverage_center(cls, item, response, children):
        item['class_'] = 'Beverage Center'
        # item['categories'] = [
        #     'Kitchen > Refrigeration > Beverage Centers'
        # ]

        # Does it mention wine racks, wine storage, or wine shelves?
        texts = response.css('#intro,#specs,#features').css('::text').extract()
        text = ' '.join(texts)
        wine_mentions = re.findall(r'wine\s+(storage|rack|shel[vf])', text)
        is_wine_storage = len(wine_mentions) > 0

        # Type and finish are based on title content.
        ltitle = item['title'].lower()
        type_ = 'Freestanding' if 'freestanding' in ltitle else 'Built-in'
        if 'panel ready' in ltitle:
            finish = 'Panel-ready'
        else:
            finish = 'Stainless Steel'

        item['attributes'].update({
            'type': ('Type', type_),
            'finish': ('Finish', finish),
            'makes_ice': ('Makes ice', False),
            'is_undercounter': ('Undercounter', True),
            'is_outdoor': ('Outdoor Compatable', False),
            'is_counter_depth': ('Counter depth', True),
            'is_wine_storage': ('Stores wine', is_wine_storage),
        })

    @classmethod
    def handle_coffee_maker(cls, item, response, children):
        item['class_'] = 'Coffee Maker'
        # item['categories'] = [
        #     'Kitchen > Cooking > Built-in Coffee Makers',
        #     ]

        # All subzero coffee makers have the following attribute values.
        item['attributes']['type'] = 'Type', 'Built-in'
        item['attributes']['is_plumbed'] = 'Plumbed', False
        item['attributes']['is_programmable'] = 'Programmable', True
        item['attributes']['dispenses_hot_water'] = 'Hot Water Dispenser', True
        item['attributes']['froths_milk'] = 'Milk Frother', True
        item['attributes']['grinds_beans'] = 'Bean Grinder', True
        item['attributes']['carafe_capacity'] = 'Carafe Capacity (oz)', 12

        # Subzero coffee makers are either stainless steel or black.
        if 'Stainless' in item['title']:
            item['attributes']['finish'] = 'Finish', 'Stainless Steel'
        else:
            item['attributes']['finish'] = 'Finish', 'Color'
            item['colors'].append({'name': 'Black', 'hexcode': '#000000'})

    @classmethod
    def handle_cooktop(cls, item, response, children):
        item['class_'] = 'Cooktop'
        # item['categories'] = [
        #     'Kitchen > Cooking > Cooktops',
        #     ]

        ltitle = item['title'].lower()

        texts = response.css('#intro,#features,#specs').css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()

        finish = 'Stainless Steel'  # All cooktops finished w/ stainless steel.
        fuel = 'Gas' if 'gas' in ltitle else 'Electric'
        uses_induction = 'induction' in ltitle
        is_framed = 'framed' in ltitle and 'unframed' not in ltitle
        has_smoothtop = 'smoothtop' in ltext

        elements = cls._get_element_count(item, response)

        item['attributes'].update({
            'finish': ('Finish', finish),
            'fuel': ('Fuel', fuel),
            'uses_induction': ('Induction', uses_induction),
            'element_count': ('Burners/Elements', elements),
            'is_framed': ('Framed', is_framed),
            'has_smoothtop': ('Smoothtop', has_smoothtop),
        })

    @classmethod
    def handle_grill(cls, item, response, children):
        item['class_'] = 'Grill'
        # item['categories'] = [
        #     'Outdoor Living > Cooking > Grills',
        #     ]

        fuel_type = 'Gas'
        type_ = 'Built-in'
        elements = cls._get_element_count(item, response, depth=3)

        item['attributes'].update({
            'fuel_type': ('Fuel Type', fuel_type),
            'type': ('Type', type_),
            'element_count': ('Number of Burners', elements),
        })

    @classmethod
    def handle_ice_maker(cls, item, response, children):
        item['class_'] = 'Ice Maker'
        # item['categories'] = [
        #     'Kitchen > Refrigeration > Ice Machines'
        # ]
        ltitle = item['title'].lower()
        is_outdoor = 'outdoor' in ltitle
        # if is_outdoor:
        #     item['categories'][0] = \
        #         'Outdoor Living > Refrigeration > Ice Machines'

        item['attributes'].update({
            'type': ('Type', 'Built-in'),
            'finish': ('Finish', 'Panel-ready'),
            'is_outdoor': ('Outdoor', is_outdoor),
        })

    @classmethod
    def handle_microwave(cls, item, response, children):
        item['class_'] = 'Microwave'
        # item['categories'] = [
        #     'Kitchen > Cooking > Built-in Microwaves',
        #     ]

        ltitle = item['title'].lower()
        uses_convection = 'convection' in ltitle

        # Type and style.
        if 'drawer' in ltitle:
            type_ = 'Built-in'
            style = 'Drawer'
        else:
            type_ = 'Freestanding'
            style = 'Door'

        # Power
        wattage = None
        texts = response.css('#features,#specs,#intro').css('::text')
        text = ' '.join(texts.extract())
        matches = re.findall(r'(\d+)\s+watts', text, re.I)
        try:
            wattage = max(map(int, matches))
        except ValueError:
            pass

        # Design style
        if 'transitional' in ltitle:
            design_style = 'Transitional'
        elif 'professional' in ltitle:
            design_style = 'Professional'
        else:
            design_style = 'Standard'

        # Capacity
        capacity = None
        text = ' '.join(response.css('#specs ::text').extract())
        lines = [l.strip() for l in text.splitlines() if ':' in l]
        for line in lines:
            if line.startswith('Capacity'):
                c, _ = line.split(':')[-1].strip().split(' ', 1)
                capacity = float(c)
                break

        item['attributes'].update({
            'style': ('Style', style),
            'design_style': ('Design Style', design_style),
            'type': ('Type', type_),
            'capacity': ('Capacity', capacity),
            'convection': ('Convection', uses_convection),
            'has_turntable': ('Has Turntable', True),
            'wattage': ('Wattage', wattage),
        })

    @classmethod
    def handle_module(cls, item, response, children):
        item['class_'] = 'Module'
        # item['categories'] = [
        #     'Kitchen > Cooking > Modules',
        #     ]

        ltitle = item['title'].lower()
        if 'burner' in ltitle:
            type_ = 'Burner'
            # item['categories'] = [
            #     'Outdoor Living > Cooking > Modules',
            # ]
        elif 'grill' in ltitle:
            type_ = 'Grill'
        elif 'fryer' in ltitle:
            type_ = 'Fryer'
        elif 'steamer' in ltitle:
            type_ = 'Steamer'
        elif 'cooktop' in ltitle:
            type_ = 'Burner'

        if 'gas' in ltitle:
            fuel = 'Gas'
        else:
            fuel = 'Electric'

        if fuel == 'Gas':
            # Create children for LP and NG.
            fuel_type = 'LP or NG'
        else:
            fuel_type = 'None'

        item['attributes'].update({
            'type': ('Type', type_),
            'fuel': ('Fuel', fuel),
            'uses_induction': ('Induction', False),
            'fuel_type': ('Fuel Type', fuel_type),
        })

    @classmethod
    def handle_oven(cls, item, response, children):
        item['class_'] = 'Oven'
        # item['categories'] = [
        #     'Kitchen > Cooking > Ovens',
        #     ]

        ltitle = item['title'].lower()

        # design_style
        if 'transitional' in ltitle:
            design_style = 'Transitional'
            finish = 'Stainless Steel'
        elif 'professional' in ltitle:
            design_style = 'Professional'
            finish = 'Stainless Steel'
        elif 'contemporary' in ltitle:
            design_style = 'Standard'
            finish = 'Color'
            item['colors'].append({
                'name': 'Black',
                'hexcode': '000000'
            })
        else:  # if 'l series' in ltitle:
            design_style = 'None'
            finish = 'Stainless Steel'

        # Capacity
        capacity = None
        text = ' '.join(response.css('#specs ::text').extract())
        lines = [l.strip() for l in text.splitlines() if ':' in l]
        for line in lines:
            if line.split(':')[0].split()[-1].startswith('Capacity'):
                c, _ = line.split(':')[-1].strip().split(' ', 1)
                capacity = float(c)
                break

        # Gas or electric (yes, gas ovens exist, apparently).
        if 'gas' in ltitle:
            fuel = 'Gas'
            fuel_type = 'LP or NG'  # FIXME: Should yield child products
        else:
            fuel = 'Electric'
            fuel_type = 'None'

        # Configuration
        if 'double' in ltitle:
            configuration = 'Double'
        elif 'triple' in ltitle:
            configuration = 'Triplestack'
        else:
            configuration = 'Single'

        # Type
        type_ = 'All Oven'

        # Convection or not.
        if 'convection' in ltitle:
            uses_convection = True
        else:
            ftext = ' '.join(response.css('#features ::text').extract())
            uses_convection = 'convection' in ftext.lower()

        # Method
        if 'combi' in ltitle:
            method = 'CombiSteam'
        elif 'steam' in ltitle:
            method = 'Steam'
        elif 'speed' in ltitle:
            method = 'Speed'
        else:
            method = 'Heat'

        # Outdoor
        is_outdoor = False

        # Self-cleaning
        more_texts = response.css('#features, #intro').css('::text').extract()
        text += ' '.join(more_texts)
        text = text.lower()
        self_cleaning = 'self clean' in text or 'self-clean' in text

        item['attributes'].update({
            'fuel': ('Fuel', fuel),
            'fuel_type': ('Fuel Type', fuel_type),
            'design_style': ('Design Style', design_style),
            'finish': ('Finish', finish),
            'method': ('Method', method),
            'configuration': ('Configuration', configuration),
            'capacity': ('Capacity', capacity),
            'uses_convection': ('Convection', uses_convection),
            'type': ('Type', type_),
            'has_self_clean': ('Self Cleaning', self_cleaning),
            'is_outdoor': ('Outdoor', is_outdoor),
        })

    @classmethod
    def handle_range(cls, item, response, children):
        item['class_'] = 'Range'
        # item['categories'] = [
        #     'Kitchen > Cooking > Ranges',
        #     ]

        ltitle = item['title'].lower()

        # Fuel
        if 'dual fuel' in ltitle:
            fuel = 'Dual Fuel'
        elif 'gas' in ltitle:
            fuel = 'Gas'
        else:
            fuel = 'Electric'

        # Options
        has_electric_griddle = False
        has_ir_griddle = 'infrared griddle' in ltitle
        has_ir_charbroiler = 'infrared charbroiler' in ltitle
        has_french_top = 'french top' in ltitle

        texts = response.css('#intro,#features,#specs').css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()

        sabbath_mode = 'star k' in ltext
        sabbath_compliance = "Yes" if sabbath_mode else "No"
        uses_convection = 'convection' in ltext
        self_cleaning = 'self clean' in ltext or 'self-clean' in ltext
        delayed_baking = 'delayed start' in ltext

        elements = cls._get_element_count(item, response)

        item['attributes'].update({
            'type': ('Type', 'Freestanding'),
            'fuel': ('Fuel', fuel),
            'has_delay_bake': (
                'Oven Features::Delayed Baking', delayed_baking),
            'element_count': ('Burners/Elements', elements),
            'uses_convection': ('Oven Features::Convection', uses_convection),
            'drawer': (
                'Oven Features::Drawer Type', 'None'),
            'has_self_clean': (
                'Oven Features::Self Cleaning', self_cleaning),
            'has_electric_griddle': (
                'Rangetop Features::Electric Griddle', has_electric_griddle),
            'has_ir_griddle': (
                'Rangetop Features::Infrared Griddle', has_ir_griddle),
            'has_ir_charbroiler': (
                'Rangetop Features::Infrared Charbroiler', has_ir_charbroiler),
            'has_french_top': (
                'Rangetop Features::French Top', has_french_top),
            'has_sabbath_mode': (
                'Star-K Compliance::Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': (
                'Star-K Compliance::Sabbath Compliant', sabbath_compliance),
        })

    @classmethod
    def handle_rangetop(cls, item, response, children):
        item['class_'] = 'Rangetop'
        # item['categories'] = [
        #     'Kitchen > Cooking > Rangetops',
        #     ]

        ltitle = item['title'].lower()
        texts = response.css('#intro,#features,#specs').css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()

        has_electric_griddle = False
        has_ir_griddle = 'infrared griddle' in ltitle
        has_ir_charbroiler = 'infrared charbroiler' in ltitle
        has_french_top = 'french top' in ltitle
        sabbath_mode = 'star k' in ltext
        sabbath_compliance = "Yes" if sabbath_mode else "No"
        has_grill = 'grill' in ltitle
        fits_wok = len(re.findall(r'\bwok\b', ltext)) > 0

        elements = cls._get_element_count(item, response)

        item['attributes'].update({
            'type': ('Type', 'Built-in'),
            'fuel': ('Fuel', 'Gas'),
            'burner_type': ('Burner Type', 'Sealed'),
            'fuel_type': ('Fuel Type', 'LP or NG'),
            'element_count': ('Burners/Elements', elements),
            'has_ir_charbroiler': ('Infrared Charbroiler', has_ir_charbroiler),
            'has_ir_griddle': ('Infrared Griddle', has_ir_griddle),
            'has_electric_griddle': ('Electric Griddle', has_electric_griddle),
            'has_grill': ('Grill', has_grill),
            'has_french_top': ('French Top', has_french_top),
            'has_wok': ('Fits Wok', fits_wok),
            'has_sabbath_mode': (
                'Star-K Compliance::Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': (
                'Star-K Compliance::Sabbath Compliant', sabbath_compliance),
        })

    @classmethod
    def handle_refrigeration(cls, item, response, children):
        """
        This does NOT handle wine storages, beverage coolers, nor ice makers.
        """
        item['class_'] = 'Refrigeration'
        # item['categories'] = [
        #     'Kitchen > Refrigeration > Refrigerators & Freezers',
        #     ]

        # Default values
        configuration = 'Column'
        type_ = 'Freestanding'
        function = 'All Refrigerator'
        shape = 'Upright'
        finish = None
        hinge = None
        width = None
        is_wine_storage = None
        is_outdoor = False
        is_undercounter = False
        is_counter_depth = False
        makes_ice = False

        # Things we will use repeatedly to make determinations.
        ltitle = item['title'].lower()
        url = urlparse(response.url)

        is_outdoor = False
        if 'outdoor' in ltitle:
            is_outdoor = True
            # item['categories'].append(
            #     'Outdoor Living > Refrigeration > Refrigerators & Freezers'
            # )

        is_undercounter = '/counter-refrigerator/' in url.path
        makes_ice = 'ice-maker' in url.path
        is_counter_depth = True

        # Does it mention wine racks, wine storage, or wine shelves?
        texts = response.css('#intro,#specs,#features').css('::text').extract()
        text = ' '.join(texts)
        n = len(re.findall(r'wine\s+(storage|rack|shel[vf])', text))
        is_wine_storage = bool(n)

        # Type and finsih (only the Pro48 series is freestanding)
        if 'pro 48' in ltitle:
            type_ = 'Freestanding'
            finish = 'Stainless Steel'
            configuration = 'Side-by-side'
            function = 'Refrigerator & Freezer'
            shape = 'Upright'
        else:
            type_ = 'Built-in'

            # Finish
            if 'integrated-fridges' in url.path:
                finish = 'Integrated'
            elif 'panel ready' in ltitle:
                finish = 'Panel-ready'
            else:
                finish = 'Stainless Steel'

            # Configuration
            if 'side-by-side' in ltitle:
                configuration = 'Side-by-side'
            elif 'over-under' in ltitle or 'over-and-under' in ltitle:
                configuration = 'Bottom-mount'
            elif 'french door' in ltitle:
                configuration = 'French Door'
            elif 'column' in ltitle:
                configuration = 'Column'
            else:
                configuration = 'Single'

            # Function (aka make up)
            if 'freezer' in ltitle:
                if 'refrigerator' in ltitle:
                    function = 'Refrigerator & Freezer'
                else:
                    function = 'All Freezer'
            else:
                function = 'All Refrigerator'

            # Shape
            if 'drawers' in ltitle:
                shape = 'Drawer'
            else:
                shape = 'Upright'

        # Hinge options will yield child items.

        item['attributes'].update({
            'type': ('Type', type_),
            'function': ('Function', function),
            'shape': ('Shape', shape),
            'configuration': ('Configuration', configuration),
            'finish': ('Finish', finish),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_wine_storage': ('Stores Wine', is_wine_storage),
            'makes_ice': ('Makes Ice', makes_ice),
        })

    @classmethod
    def handle_ventilation(cls, item, response, children):
        item['class_'] = 'Ventilation'
        # item['categories'] = [
        #     'Kitchen > Ventilation > Vent Hoods',
        #     ]

        ltitle = item['title'].lower()
        texts = response.css('#intro,#features,#specs').css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()

        type_ = None
        if 'wall hood' in ltitle or 'wall chimney' in ltitle:
            type_ = 'Wall Mount'
        elif 'island hood' in ltitle:
            type_ = 'Island'
        elif 'downdraft' in ltitle:
            type_ = 'Downdraft'
        elif 'hood liner' in ltitle or 'hood insert' in ltitle:
            type_ = 'Insert'
        elif 'under' in ltitle and 'cabinet' in ltitle:
            type_ = 'Under Cabinet'

        if type_ == 'Insert':
            finish = 'Insert'
        else:
            finish = 'Stainless Steel'  # assumption
            if 'black' in ltitle:
                finish = 'Color'
                item['colors'].append({
                    'name': 'Black',
                    'hexcode': '000000'
                })

        venting = 'Ducted'  # FIXME: is this always the case?
        if 'retractable' in ltext:
            movement = 'Retractable'
        elif re.search(r'\b(rise|lower)s\b', ltext):
            movement = 'Ascending'
        else:
            movement = 'Fixed'

        exhaust = None

        item['attributes'].update({
            'type': ('Type', type_),
            'finish': ('Finish', finish),
            'venting': ('Venting', venting),
            'movement': ('Movement', movement),
        })

    @classmethod
    def handle_warming_drawer(cls, item, response, children):
        item['class_'] = 'Warming Drawer'
        # item['categories'] = [
        #     'Kitchen > Cooking > Warming Drawers',
        #     ]

        ltitle = item['title'].lower()
        texts = response.css('#intro,#features,#specs').css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()

        is_cup_and_plate = 'cup warming' in ltitle
        is_outdoor = 'outdoor' in ltitle

        if 'black' in ltitle:
            finish = 'Color'
            item['colors'].append({
                'name': 'Black',
                'hexcode': '000000'
            })
        else:
            finish = 'Stainless Steel'

        sabbath_mode = 'star k' in ltext
        uses_convection = 'convection' in ltext

        # Capacity
        capacity = None
        text = ' '.join(response.css('#specs ::text').extract())
        lines = [l.strip() for l in text.splitlines() if ':' in l]
        for line in lines:
            if line.split(':')[0].split()[-1].startswith('Capacity'):
                c, _ = line.split(':')[-1].strip().split(' ', 1)
                capacity = float(c)
                break

        item['attributes'].update({
            'is_cup_plate': ('For Cups and Plates', is_cup_and_plate),
            'is_outdoor': ('Outdoor', is_outdoor),
            'finish': ('Finish', finish),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'uses_convection': ('Convection', uses_convection),
            'capacity': ('Capacity', capacity),
        })

    @classmethod
    def handle_wine_storage(cls, item, response, children):
        item['class_'] = 'Wine Storage'
        # item['categories'] = [
        #     'Kitchen > Refrigeration > Wine Coolers',
        #     ]
        specs = response.css("#specs *::text").extract()
        specs = '\n'.join([x for x in specs if len(x.strip())])

        # Figure out type
        ltitle = item['title'].lower()
        type_ = "Built-in"
        if 'freestanding' in ltitle:
            type_ = 'Freestanding'

        # Figure out bottle capacity
        features = response.css(".accordion-list__sublist-item ::text").extract()
        bottle_capacity = 0
        if 'Wine Storage Capacity: ' in features:
            bc_idx = features.index('Wine Storage Capacity: ') + 1
            bottle_capacity = int(features[bc_idx].split()[0])

        # Figure out number of temperature zones
        temperature_zones = 1
        depth = 3
        count_re = '\d+|%s' % '|'.join(utils.numbers)
        target_re = '(temperature zone)'
        extra_re = '(\s+[\w-]+){0,%d}' % depth
        pattern_ = r'(?P<n>%s)%s\s+%s'
        pattern = pattern_ % (count_re, extra_re, target_re)
        regex = re.compile(pattern, re.I)
        candidates = response.css('#intro,#specs,#features').css('::text')
        likely_text = ' '.join(candidates.extract()).lower()
        greatest = 0
        for card, _, _ in regex.findall(likely_text):
            try:
                card = int(card)
            except ValueError:
                card = utils.numbers.get(card, 0)
            if card > greatest:
                greatest = card
        if greatest:
            temperature_zones = greatest

        # Figure out configuration
        configuration = "Single"
        if 'column' in ltitle:
            configuration = "Column"

        # Figure out finish (the order of the if statements is important)
        finish = "Stainless Steel"
        if "panel ready" in ltitle:
            finish = "Panel-ready"

        if "integrated" in ltitle:
            finish = "Integrated"

        # Figure out is_outdoor
        # SubZero wine storage is always indoor
        is_outdoor = False

        # Figure out is_undercounter
        is_undercounter = False
        if "undercounter" in ltitle:
            is_undercounter = True

        # Figure out is_counter_depth
        is_counter_depth = True

        # Figure out if its energy star rated or not
        energy_star_rated = False
        if "energy star certified" in specs.lower():
            energy_star_rated = True

        # Figure out if its energy star rated or not
        sabbath_compliance = "No"
        sabbath_mode = False
        if "star k certified" in specs.lower():
            sabbath_compliance = "Yes"
            sabbath_mode = True

        item['attributes'].update({
            'type': ('Type', type_),
            'bottle_capacity': ('Bottle Capacity', bottle_capacity),
            'temperature_zones': ('Temperature Zones', temperature_zones),
            'configuration': ('Configuration', configuration),
            'finish': ('Finish', finish),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_energy_rated': ('Energy-Star Rated', energy_star_rated),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': ('Sabbath Compliance', sabbath_compliance),
        })
