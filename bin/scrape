#!/usr/bin/python
import os
import re
import sys
import json
import shlex
import magic
import shutil
import mimetypes
import unicodedata

from colorama import init, Fore, Back, Style
from subprocess import Popen, PIPE

init(autoreset=True)

MAIN_STYLE = Fore.MAGENTA + Style.BRIGHT
GOOD_STYLE = Fore.GREEN + Style.BRIGHT
BAD_STYLE = Fore.RED + Style.BRIGHT
WARN_STYLE = Fore.YELLOW + Style.DIM
INFO_STYLE = Fore.BLUE + Style.BRIGHT

HEADER_TEXT = MAIN_STYLE +  '''
===========================
  Initializing {name}
---------------------------
'''


def slugify(value):
    value = unicodedata.normalize('NFKD', unicode(value)).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s_-]', '', value).strip().lower()
    return re.sub('[-\s]+', '_', value)


class FileRenamer(object):
    def __init__(self, filepath):
        with open(filepath) as f:
            self.items = json.loads(f.read())
        self.base_location = os.path.dirname(filepath)
        self.process()
        print "Files renamed!"

    def process(self):
        for item in self.items:
            self.rename_files(item)
            self.rename_images(item)

    def rename_files(self, item):
        file_urls = item['file_urls']
        for file_ in item['files']:
            filepath = os.path.join(self.base_location, "files", file_['path'])
            maker = item['maker']
            upc = item['upc']
            type_ = file_urls[file_['url']]['name']
            ext = mimetypes.guess_all_extensions(
                magic.from_file(filepath, mime=True))[-1]
            filename = "{maker}_{upc}_{type}".format(
                maker=maker,
                upc=upc,
                type=type_)
            filename = "full/" + slugify(filename) + ext
            file_['path'] = filename
            new_filepath = os.path.join(self.base_location, "files", filename)
            shutil.copy(filepath, new_filepath)
            print "Copying {} to {}".format(filepath, new_filepath)

    def rename_images(self, item):
        images = item['images']
        for idx, image in enumerate(images):
            filepath = os.path.join(self.base_location, "images", image['path'])
            maker = item['maker']
            upc = item['upc']
            ext = mimetypes.guess_all_extensions(
                magic.from_file(filepath, mime=True))[-1]
            filename = "{maker}_{upc}_{idx}".format(
                maker=maker,
                upc=upc,
                idx=idx)
            filename = "full/" + slugify(filename) + ext
            image['path'] = filename
            new_filepath = os.path.join(self.base_location, "images", filename)
            shutil.copy(filepath, new_filepath)
            print "Copying {} to {}".format(filepath, new_filepath)

def get_filenames(spider_name, folder="scrape_data"):
    filename = '{}.json'.format(spider_name.lower())
    filename = os.path.join(folder, filename)
    return filename, filename + '~'

def restore_original(backup, filename):
    print WARN_STYLE + "       Restoring original JSON file...",
    try:
        os.rename(backup, filename)
    except OSError:
        print BAD_STYLE + "nope, couldn't even do that right :-("
    else:
        print WARN_STYLE + "done\n"

def create_backup(filename, backup):
    print INFO_STYLE + " - Creating backup of {}...".format(filename),
    try:
        os.rename(filename, backup)
    except OSError:
        print WARN_STYLE + "couldn't find it, but no biggie :-)"
    else:
        print GOOD_STYLE + "saved!"

def run_scraper(spider_name, filename):
    cmdline = 'scrapy crawl {} -o {}'.format(spider_name, filename)
    print INFO_STYLE + " - Running '{}'".format(cmdline)
    proc = Popen(shlex.split(cmdline), stdout=PIPE)
    try:
        while not proc.poll():
            pass
    except KeyboardCtrlEvent:
        # Kill with fire.
        proc.terminate()
        proc.kill()
        print BAD_STYLE + "\n\nScraping canceled!"
        return None
    return proc.returncode

def scrape_one(spider_name):
    print HEADER_TEXT.format(name=spider_name)

    # Backup the original JSON file.
    filename, backup = get_filenames(spider_name)
    create_backup(filename, backup)

    # Launch the scraper. Allow Ctrl-C to kill it immediately.
    cmdline = 'scrapy crawl {} -o {}'.format(spider_name, filename)
    print INFO_STYLE + " - Running '{}'".format(cmdline)
    proc = Popen(shlex.split(cmdline), stdout=PIPE)
    try:
        # Not using wait() because of lengthy output.
        while proc.poll() is None:
            pass
    except KeyboardInterrupt:
        # Kill with fire.
        proc.terminate()
        proc.kill()
        print BAD_STYLE + "\n\nScraping canceled!"
        restore_original(backup, filename)
        return
    except Exception as e:
        print BAD_STYLE + "\n\nERROR: Scraping didn't go as planned..."
        print BAD_STYLE + str(e)
        restore_original(backup, filename)

    if proc.returncode == 0:
        print GOOD_STYLE + "Scraping Complete!"
        # renamer = FileRenamer(filename)
        # items = renamer.items
        # with open(filename) as f:
        #     orig_items = json.loads(f.read())

        # with open(filename+".unrenamed", 'w') as f:
        #     f.write(json.dumps(orig_items))

        # with open(filename, 'w') as f:
        #     f.write(json.dumps(items))

        # print GOOD_STYLE + "JSON successfully updated with new image/file paths!"
    else:
        r = proc.returncode
        print BAD_STYLE + "\n\nERROR: Something went wrong (returncode=%s)" % r
        restore_original(backup, filename)


if __name__ == '__main__':
    spider_names = sys.argv[1:]
    for spider_name in spider_names:
        scrape_one(spider_name)

